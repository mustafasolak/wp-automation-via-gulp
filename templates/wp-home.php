<?php get_header() ?>

  <main id="blog">

    <section>
      <? if (have_posts() ) : while (have_posts()) : the_post() ?>
        <article>
          <h2><? the_title() ?></h2>
          <? the_content() ?>
        </article>
      <? endwhile; endif; ?>
    </section>

  </main>

<?php get_footer() ?>
