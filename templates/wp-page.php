<?php get_header() ?>

<main id="page">

  <section>
    <? while (have_posts()) : the_post() ?>
      <article>
        <h1><? the_title() ?></h1>
        <? the_content() ?>
      </article>
    <? endwhile; ?>
  </section>

</main>

<?php get_footer() ?>
