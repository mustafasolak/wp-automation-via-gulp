<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1, initial-scale=1">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php wp_head() ?>
  </head>
  <body>
    <header>
      <nav>

        <a href="<?php get_bloginfo('url'); ?>">
          <div class="logo"></div>
        </a>


      </nav>

    </header>
